﻿using UnityEngine;
using System.Collections;

public class World : MonoBehaviour {

	public GameObject level;
	public GameObject player;
	public float speed = 2;
	public int angleTollerance;
	public static float[] rotation = new float[3];

	private float value;
	private Gyroscope gyro;
	private bool canStartLevel = false;
	private float screenHalf = Screen.width / 2;

	// Use this for initialization
	void Start () {
		gyro = Input.gyro;
		if(!gyro.enabled)
		{
			gyro.enabled = true;
		}

		if (SystemInfo.deviceType == DeviceType.Handheld) {
			//level.transform.RotateAround (player.transform.position, Vector3.forward, Input.gyro.attitude.eulerAngles.z);
			UpdateGameAngles ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		float delta = Time.deltaTime;


		//rotate the world
		//Is it on device or on desktop
		if (SystemInfo.deviceType == DeviceType.Handheld) {
			
			if (Input.touchCount > 0) {
				RotateWorld (Input.GetTouch (0).position.x > screenHalf, delta);
			}

			UpdateGameAngles ();
		} 
		else {
			
			if (Input.GetButton("Fire1")) {
				RotateWorld (Input.mousePosition.x > screenHalf, delta);
			}

			UpdateGameAngles ();
		}
	}

	void UpdateGameAngles() {
		GameController.worldZAngle = Input.gyro.attitude.eulerAngles.x;
		GameController.deviceZAngle = Input.gyro.attitude.eulerAngles.z;
		GameController.canStartLevel = canStartLevel;
	}

	void RotateWorld(bool rotateRight,float delta) {
		if(rotateRight) {
			//Rotate right
			level.transform.RotateAround(player.transform.position, Vector3.forward, -1 * delta * speed);
		}
		else {
			//Rotate left
			level.transform.RotateAround(player.transform.position, Vector3.forward, 1 * delta * speed);
		}
	}
}
