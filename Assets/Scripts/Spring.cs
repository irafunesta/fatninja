﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Spring : MonoBehaviour {

	public float thrust;

	//private Collider2D collider;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D other) 
	{
		print("Entered trigger :" + other.gameObject.name);
		if (other.gameObject.name == "Player") {
			Rigidbody2D rb = other.gameObject.GetComponent<Rigidbody2D>();
			rb.AddForce (Vector2.up * thrust);
		}
	}
}
