﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {
	
	public float thrust = 200.0f;
	private Rigidbody2D rb;
	private bool canJump;
	
	//Awake is always called before any Start functions
	void Awake()
	{
		
	}

	// Use this for initialization
	void Start () {
		canJump = true;
		rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {	

		if (SystemInfo.deviceType != DeviceType.Handheld) {
			if (Input.GetButtonDown ("Fire1") && canJump) {
				if(GameController.IsDebug()) {
					ReloadLevel ();
				}
				else {
					Jump ();
				}
			}
		}else {
			//Input jump on device
			if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began && canJump) {
				Jump ();
			}
		}
		
	}

	void OnCollisionEnter2D (Collision2D other) {
		if (other.gameObject.name == "level") {
			canJump = true;
		}

		print ("collsion with: " + other.gameObject.name);
	}

	void Jump (){
		//rb.AddForce (Vector2.up * thrust);
		//canJump = false;
	}

	void ReloadLevel(){
		SceneManager.LoadScene ("main");
	}
}
