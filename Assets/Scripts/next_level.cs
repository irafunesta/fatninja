﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class next_level : MonoBehaviour {

	public string nextLevel;
	//private Collider2D collider;
	// Use this for initialization
	void Start () {
		this.GetComponent<BoxCollider2D> ().enabled = false;
		this.GetComponent<SpriteRenderer> ().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D other) 
	{
		print("Entered trigger :" + other.gameObject.name);
		if (other.gameObject.name == "Player") {
			SceneManager.LoadScene (nextLevel);
		}
	}
}
