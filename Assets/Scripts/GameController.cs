﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{	
	public Text coinsCounter;
	public Text uiLog;
	public int totalCoins;
	public bool g_debug;

	private static int playerCoins;
	public static float deviceZAngle;
	public static float worldZAngle;
	public static bool debug = false;
	public static bool canStartLevel;

	private static ArrayList log = new ArrayList();

	// Use this for initialization
	void Start () {
		playerCoins = 0;
		debug = g_debug;
	}
	
	// Update is called once per frame
	void Update () 
	{		
		if(coinsCounter != null)
		{
			coinsCounter.text = "Coins " + playerCoins + "/" + totalCoins;
		}

		printLog (uiLog);

		if (playerCoins == totalCoins) 
		{
			//Open the door for the next level
			GameObject door = GameObject.Find("door");
			door.GetComponent<BoxCollider2D> ().enabled = true;
			door.GetComponent<SpriteRenderer> ().enabled = true;
		}
	}

	public static void AddPlayerCoins(int num)
	{
		updateLog ("Player Added Coin");
		playerCoins += num;
	}

	public static void SetPlayerCoins(int num) 
	{
		playerCoins = num;
	}

	public static int GetPlayerCoins()
	{
		return playerCoins;
	}

	public static bool IsDebug() {
		return debug;
	}

	public void ActivateDebug() {
		debug = !debug;
	}

	public void ReloadLevel() {
		updateLog ("Reload Level");
		SceneManager.LoadScene (SceneManager.GetActiveScene().name);
	}

	public void ExitGame() {
		Debug.Log ("Exit game");
		Application.Quit();
	}

	private static void updateLog(string newLine){
		if(log.Count >= 3) {
			//Swap the log to free the last 3 elements
			for(int i = 1; i < log.Count; i++){
				log [i - 1] = log [i];
			}
			log [log.Count-1] = newLine;
		}
		else
		{
			log.Add (newLine);
		}
	}

	private static void printLog(Text screen) {
		if(screen != null)
		{
			string fullLog = "";
			for(int i = 0; i< log.Count; i++){
				fullLog += log[i] + "\n";
			}
			screen.text = fullLog;
		}
	}
}
