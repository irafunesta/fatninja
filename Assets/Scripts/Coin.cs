﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D other) {
		print("Entered trigger :" + other.gameObject.name);
		if (other.gameObject.name == "Player") {
			GameController.AddPlayerCoins (1);
			DestroyObject (this.gameObject);
		}
	}
}
